var $table = $("#taskTable");
var tableDataTable = null;
var token = $("input[name='_token']").val();
$(document).ready(function () {
    intinalizeClickEvent();
    initializeLoadEvent();
    initializeSubmitEvent();
});
function initializeLoadEvent() {
    initializeDatatable();
}

function intinalizeClickEvent() {

    $(document).on('click', '.deleteBtn', function () {
        debugger;
        var id = $(this).attr("data-id");
        $.post(window.deleteTask, {
            "id": id,
            "_token": token,
        }, function (dataObject) {

            var data = $.parseJSON(dataObject);
            if (data.status == "0") {
                tableDataTable.ajax.reload();
                toastr["success"]("Deleted Successfully");
            } else {
                toastr["error"]("Please try again...");
            }
            $('#con-close-modal').modal('hide');
        });
    });

    $(document).on('click', '.successBtn', function () {
        debugger;
        var id = $(this).attr("data-id");
        var status = $(this).attr("data-status");
        $.post(window.taskCompleted, {
            "id": id,
            "status": status,
            "_token": token,
        }, function (dataObject) {

            var data = $.parseJSON(dataObject);
            if (data.status == "0") {
                tableDataTable.ajax.reload();
                if (status == "0") {
                    toastr["success"]("Task Completed Successfully");
                } else {
                    toastr["success"]("You have set your task to pending");
                }

            } else {
                toastr["error"]("Please try again...");
            }
            $('#con-close-modal').modal('hide');
        });
    });
}
function initializeSubmitEvent() {

    $("#Frm_taskDetails").submit(function (e) {
        e.preventDefault();
        submitForm();
    });

    $("#createTask").click(function () {
        debugger;
        $("#Frm_taskDetails").removeAttr("data-id");
        $(".modal-title").html("Create Task");
        $("#Frm_taskDetails").trigger("reset");
    });
}

function submitForm() {
    debugger;
    var title = $("#taskTitle").val();
    var desc = $("#description").val();
    var date = $("#taskDate").val();
    var time = $("#taskTime").val();
    var id = $("#Frm_taskDetails").attr("data-id");

    var priority = $("input[name='radio']:checked").val();
    $.post(window.upsertTask, {
        "_token": token,
        "title": title,
        "desc": desc,
        "date": date,
        "time": time,
        "priority": priority,
        "id": id

    }, function (dataObject) {

        var data = $.parseJSON(dataObject);
        if (data.status == "0") {
            tableDataTable.ajax.reload();
            if (id) {
                toastr["success"]("Task Updated Successfully");

            } else {
                toastr["success"]("Task Created Successfully");
            }

        } else {
            toastr["error"]("Please try again...");
        }
        $('#con-close-modal').modal('hide');
    });
}

function initializeDatatable() {

    if (tableDataTable) {
        return false;
    }

    tableDataTable = $table.on('preXhr.dt', function (e, settings, data) {
//        $table.LoadingOverlay("show");
    }).on('draw.dt', function () {
//        $table.LoadingOverlay("hide");
    }).DataTable({
        processing: true,
        serverSide: true,
        searching: true,
        order: [[3, 'desc']],
//        pageLength: 25,
        "rowCallback": function (row, data) {
            debugger;
            if (data.finished == 1) {
                $(row).addClass('strikeout');
            }
        },
        ajax: {
            url: window.getTasks,
//            data: function (d) {
//                d.fulfillment_status = fulfillment_status;
//                return d;
//            }
        },
        columnDefs: [
            {
                name: "id",
                data: "id",
                className: "text-center",
                targets: [0],
                visible: false,
                render: function (data, type, row, meta) {
                    var return_div = $("<div class='table-data-design '>");
                    return_div.html(data);
                    return return_div.html();
                }
            },
            {
                name: "title",
                data: "title",
                className: "text-center",
                targets: [1],
                visible: true,
                render: function (data, type, row, meta) {

                    var return_div = $("<div class='table-data-design '>");
                    return_div.html(data);
                    return return_div.html();
                }
            },
            {
                name: "description",
                data: "description",
                className: "text-center",
                targets: [2],
                visible: true,
                render: function (data, type, row, meta) {
                    var return_div = $("<div class='table-data-design '>");
                    return_div.html(data);
                    return return_div.html();
                }
            },
            {
                name: "time",
                data: "time",
                className: "text-center",
                targets: [3],
                visible: true,
                render: function (data, type, row, meta) {
                    var return_div = $("<div class='table-data-design '>");
                    return_div.html(data);
                    return return_div.html();
                }
            },
            {
                name: "priority",
                data: "priority",
                className: "text-center",
                targets: [4],
                visible: true,
                sWidth: "10%",
                render: function (data, type, row, meta) {
                    debugger;
                    var return_div = $("<div class='table-data-design '>");
                    if (data == "high") {
                        return_div.html("<span class='badge badge-danger float-right'>High Priority</span>");
                    } else if (data == "low") {
                        return_div.html("<span class='badge badge-warning float-right'>Low Priority</span>");
                    } else {
                        return_div.html("<span class='badge badge-primary float-right'>Medium Priority</span>");
                    }
                    return return_div.html();
                }
            },
            {
                name: "finished",
                data: "finished",
                className: "text-center",
                targets: [5],
                visible: true,
                sWidth: "10%",
                render: function (data, type, row, meta) {
                    var return_div = $("<div class='table-data-design '>");
                    if (data == 0) {
                        return_div.html("Pending");
                    } else {
                        return_div.html("Finished");
                    }

                    return return_div.html();
                }
            },
            {
                name: "id",
                data: "id",
                className: "text-center",
                targets: [6],
                visible: true,
                sWidth: "20%",
                render: function (data, type, row, meta) {

                    var return_div = $("<div class='table-data-design '>");
                    var main_div = $("<div class='button-setting '>");

                    if (row.finished == 0) {
                        var finishBtn = $("<button>");
                        finishBtn.addClass("fe-check successBtn btn btn-success btn-space  ");
                        finishBtn.attr("data-id", data);
                        finishBtn.attr("data-status", row.finished);
                        main_div.append(finishBtn);
                    } else {
                        var finishBtn = $("<button>");
                        finishBtn.addClass("fe-x successBtn btn btn-warning btn-space  ");
                        finishBtn.attr("data-id", data);
                        finishBtn.attr("data-status", row.finished);
                        main_div.append(finishBtn);
                    }


                    var updateBtn = $("<button>");
                    updateBtn.addClass("fe-edit updateBtn btn btn-primary btn-space ");
                    updateBtn.attr("data-id", data);
                    updateBtn.attr("data-row", JSON.stringify(row));
                    if (row.finished == 1) {
                        updateBtn.prop('disabled', true);
                    } else {
                        updateBtn.prop('disabled', false);
                    }
                    main_div.append(updateBtn);

                    var deleteBtn = $("<button>");
                    deleteBtn.addClass("fe-trash-2 deleteBtn btn btn-danger btn-space  ");
                    deleteBtn.attr("data-id", data);
                    main_div.append(deleteBtn);
                    return_div.append(main_div);
                    return return_div.html();
                }
            },
        ],

    });
}

$(document).on('click', '.updateBtn', function () {
    debugger;

    var id = $(this).attr("data-id");

    $(".modal-title").html("Update Task");

    var row = $(this).attr("data-row");
    row = $.parseJSON(row);
    action = "update";
    taskId = row.id;
    var time = row.time;
    time = time.split(" ");
    var priority = row.priority;
    $('#con-close-modal').modal('show');
    $("#taskTitle").val(row.title);
    $("#description").val(row.description);
    $("#taskDate").val(time[0]);
    $("#taskTime").val(time[1]);
    $("input[name=radio][value=" + priority + "]").attr('checked', 'checked');
    $("#Frm_taskDetails").attr("data-id", id);

});
