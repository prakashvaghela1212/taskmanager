<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Yajra\Datatables\Datatables;
use Auth;

class TaskController extends Controller {

    /*
     * Author:Prakash
     * @Description:this method is used to create or update task 
     * @Argument:will take argumne id if record for update else insert
     * 
     */
    public function upsertTask(Request $request) {
        $retObj = new \stdClass();
        try {
            $uid = Auth::user()->id;
            $date = $request->date;
            $time = $request->time;
            $id = $request->id;

            $date = $date . " " . $time;
            if (isset($id)) {
                DB::table('task_master')
                        ->where('id', $request->id)
                        ->where('user_id', $uid)
                        ->update([
                            "user_id" => $uid, "title" => $request->title, "description" => $request->desc, "time" => $date, "priority" => $request->priority
                ]);
                $retObj->status = "0";
                $retObj->message = "Sucessfully Task Updated";
            } else {
                $time = $time . ":00";
                DB::table('task_master')
                        ->insert([
                            "user_id" => $uid, "title" => $request->title, "description" => $request->desc, "time" => $date, "priority" => $request->priority
                ]);
                $retObj->status = "0";
                $retObj->message = "Sucessfully Task Inserted";
            }
        } catch (\Exception $e) {
            $retObj->status = "1";
            $retObj->message = $e;
        }
        return json_encode($retObj);
    }
/**
 * @description:this method will return all the task from DB
 * @return type tasklist
 */
    public function getTasks() {
        $uid = Auth::user()->id;
        $taskList = DB::table('task_master')->where('user_id', $uid)->get();
        return Datatables::of($taskList)->make(true);
    }
/**
 * @description this method will delete task 
 * @param Request $request
 * @return type
 */
    public function deleteTask(Request $request) {
        $retObj = new \stdClass();
        try {
            $id = $request->id;
            $uid = Auth::user()->id;

            $taskList = DB::table('task_master')->where('user_id', $uid)->where('id', '=', $id)->delete();
            if ($taskList) {
                $retObj->status = "0";
            } else {
                $retObj->status = "1";
            }
        } catch (\Exception $e) {
            $retObj->status = "1";
            $retObj->message = $e;
        }
        return json_encode($retObj);
    }
/**
 * @description this method will set task from pending to complete or visa versa
 * @param Request $request
 * @return type
 */
    public function taskCompleted(Request $request) {
        $retObj = new \stdClass();
        try {
            $id = $request->id;
//            this status used to check task is already completed or pending from jquery
            $status = $request->status;
            $uid = Auth::user()->id;
            if ($status == "0") {
                $taskList = DB::table('task_master')->where('id', '=', $id)->where('user_id', $uid)->update(['finished' => 1]);
            } else {
                $taskList = DB::table('task_master')->where('id', '=', $id)->where('user_id', $uid)->update(['finished' => 0]);
            }


            if ($taskList) {
                $retObj->status = "0";
            } else {
                $retObj->status = "1";
            }
        } catch (\Exception $e) {
            $retObj->status = "1";
            $retObj->message = $e;
        }
        return json_encode($retObj);
    }

}
