
<!--========================-->

<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from coderthemes.com/ubold/layouts/material/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Sep 2019 09:59:09 GMT -->
    <head>
        <meta charset="utf-8" />
        <title>Comparionify</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- App css -->
        <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/app.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{asset('css/plugin/toastr.min.css')}}?t=<?php echo time(); ?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css" />
        <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css" />

    </head>

    <body>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Topbar Start -->
            <div class="navbar-custom">
                <ul class="list-unstyled topnav-menu float-right mb-0">



                    <li class="dropdown notification-list">
                        <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <!--<img src="assets/images/users/user-1.jpg" alt="user-image" class="rounded-circle">-->
                            <span class="pro-user-name ml-1">
                                {{Auth::user()->name}} <i class="mdi mdi-chevron-down"></i> 
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right profile-dropdown ">

                            <a href="{{url('logout')}}" class="dropdown-item notify-item">
                                <i class="fe-log-out"></i>
                                <span>Logout</span>
                            </a>

                        </div>
                    </li>
                </ul>


                <div class="logo-box">
                    <a href="@php {{ echo url('dashboard'); }} @endphp" class="logo text-center">
                        <span class="logo-lg">
                            <span class="logo-lg-text-light">Task Manager</span>
                        </span>
                        <span class="logo-sm">
                            <img src="assets/images/logo-sm.png" alt="" height="24">
                        </span>
                    </a>
                </div>

                <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
                    <li>
                        <button class="button-menu-mobile waves-effect waves-light">
                            <i class="fe-menu"></i>
                        </button>
                    </li>




                </ul>
            </div>
            <!-- end Topbar -->

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left-side-menu">

                <div class="slimscroll-menu">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">

                        <ul class="metismenu" id="side-menu">

                            <li class="menu-title">Navigation</li>


                            <?php
                            $menu_array = array(
                                array(
                                    "display" => "Manage Task",
                                    "icon" => 'fe-airplay',
                                    "url" => "manageTask",
                                    "menu" => ""
                                ),
                            );

                            foreach ($menu_array as $menuItems) {
                                $display = $menuItems['display'];
                                $icon = $menuItems['icon'];
                                $url = $menuItems['url'];
                                $menu = null;
                                if (isset($menuItems['menu'])) {
                                    $menu = $menuItems['menu'];
                                }
                                ?>
                                <li>

                                    <?php if ($menu) { ?>
                                        <a class="{{ $url }}" >
                                            <i class="@php {{echo $icon; }} @endphp" aria-hidden="true"></i>
                                            <span> <?php echo $display ?> </span>
                                            <span class="menu-arrow"></span>

                                        </a>
                                    <?php } else { ?>
                                        <a class="waves-effect" href=" @php {{ echo url($url); }} @endphp">
                                            <i class="@php {{echo $icon; }} @endphp" aria-hidden="true"></i>
                                            <span> <?php echo $display ?> </span>
                                        </a> 
                                    <?php } ?>
                                    <!--sub menu-->

                                    <?php if ($menu != null) { ?>
                                        <ul class="nav-second-level">
                                            <?php
                                            foreach ($menu as $childMenu) {
                                                $display = $childMenu['display'];
                                                $url = $childMenu['url'];
                                                ?>
                                                <li>
                                                    <a href=" @php {{ echo url($url); }} @endphp">
                                                        <?php echo $display; ?>
                                                    </a>
                                                </li>
                                                <?php
                                            }
                                            ?>   
                                        </ul>
                                        <?php
                                    }
                                    ?>
                                    <!--end sub menu-->
                                </li>

                                <?php
                            }
                            ?>



                        </ul>

                    </div>
                    <!-- End Sidebar -->

                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->
            <div class="" style="margin-top: 5%;">
                @yield('content')
            </div>
            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->

        </div>  
        <!-- END wrapper -->



        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- Vendor js -->
        <script src="{{ asset('assets/js/vendor.min.js') }}"></script>

        <!-- App js-->
        <script src="{{ asset('assets/js/app.min.js') }}"></script>

        <script src="{{asset('js/plugin/toastr.min.js')}}?t=<?php echo time(); ?>"></script>
        <script src="{{asset('js/plugin/swal.min.js')}}?t=<?php echo time(); ?>"></script>
        <script src="{{asset('js/plugin/loaderOverlay.js')}}?t=<?php echo time(); ?>"></script>
        <script src="//cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
        <script src="//cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
        <script src="//cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>

        @yield('js')
    </body>


</html>