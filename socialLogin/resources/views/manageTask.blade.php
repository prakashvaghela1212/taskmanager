@extends('layouts.app')

@section('content')
<style>
    .btn-space{
        margin:1px;
    }
    td { position: relative; }

    tr.strikeout td:before {
        content: " ";
        position: absolute;
        top: 29%;
        left: 0;
        border-bottom: 1px dashed #111;
        width: 100%;
    }

    tr.strikeout td:after {
        content: "\00B7";
        font-size: 1px;
    }
    .badge {
        font-size: 90% !important;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-sm-12 text-right">
            <button type="button" class="btn btn-pink waves-effect waves-light" data-toggle="modal" data-target="#con-close-modal" id="createTask">Create Task</button>
        </div>
    </div>
    <div class="row" style="margin-top:10px;">
        <div class="col-12">
            <!-- Portlet card -->
            <div class="card">
                <div class="card-body">
                    <table class="table table-striped" id="taskTable">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Due Date</th>
                                <th>Priority</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div> <!-- end card-->
        </div>
    </div>

</div>

<!--Modal Start-->
<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <form id="Frm_taskDetails">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Create Task</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body p-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-1" class="control-label">Task Title</label>
                                <input type="text" id="taskTitle" class="form-control" id="field-1" placeholder="Meeting AT 4PM" required="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-3" class="control-label">Title Description</label>
                                <textarea class="form-control" id="description" placeholder="Description About your task" required=""></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-4" class="control-label">Date</label>
                                <input type="date" id="taskDate" class="form-control " placeholder="Basic datepicker" required="" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-4" class="control-label">Time</label>
                                <input class="form-control" id="taskTime" type="time" name="time" required="">
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group no-margin">
                                <label for="field-7" class="control-label">Task Priority</label>

                                <div class="col-sm-6">


                                    <div class="radio radio-danger mb-2">
                                        <input type="radio" name="radio" id="prio_high" value="high" checked>
                                        <label for="prio_high">
                                            High Priority
                                        </label>
                                    </div>

                                    <div class="radio radio-warning mb-2">
                                        <input type="radio" name="radio" id="prio_medium" value="medium" >
                                        <label for="prio_medium">
                                            Medium Priority
                                        </label>
                                    </div>


                                    <div class="radio radio-success mb-2">
                                        <input type="radio" name="radio" id="prio_low" value="low">
                                        <label for="prio_low">
                                            Low Priority
                                        </label>
                                    </div>


                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info waves-effect waves-light">Save changes</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!--EndModal-->


@csrf
@section('js')
<!--all jquery and ajax call are defined here only-->
<script src="{{asset('assets/js/app/manageTask.js')}}?t=<?php echo time(); ?>"></script>
<script>

    window.upsertTask = "{{ url("upsertTask") }}";
    window.getTasks = "{{ url("getTasks") }}";
    window.deleteTask = "{{ url("deleteTask") }}";
    window.taskCompleted = "{{ url("taskCompleted") }}";
    window.uninstallCode = "{{ url("uninstallCode") }}";
</script>
@stop


@endsection
