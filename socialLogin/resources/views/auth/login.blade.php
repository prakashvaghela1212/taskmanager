
@extends('layouts.loginApp')

@section('content')
<form class="form-horizontal" method="POST" action="{{ route('login') }}">
    {{ csrf_field() }}

    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <label for="email" class="mb-3 control-label">E-Mail Address</label>

        <div class="form-group mb-3">
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

            @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class=" form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <label for="password" class="mb-3 control-label">Password</label>

        <div class="form-group mb-3">
            <input id="password" type="password" class="form-control" name="password" required>

            @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
        </div>
    </div>



    <div class="form-group">
        <div class="form-group mb-3">
            <button type="submit" class="btn btn-primary">
                Login
            </button>
        </div>
    </div>


    <div class="text-center">
        <h5 class="mt-3 text-muted">Sign in with</h5>
        <ul class="social-list list-inline mt-3 mb-0">
            <li class="list-inline-item">
                <a href="{{url('/redirect')}}" class="social-list-item border-primary text-primary"><i class="mdi mdi-facebook"></i></a>
            </li>
        </ul>
    </div>
</form>
@endsection