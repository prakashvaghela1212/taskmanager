<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('auth.login');
});
Route::get('/register', function () {
    return view('auth.register');
});
Auth::routes();

Route::get('/redirect', 'SocialAuthFacebookController@redirect');
Route::get('/callback', 'SocialAuthFacebookController@callback');


Route::group(['middleware' => 'checkLogin'], function () {
    Route::get('/manageTask', function() {
        return view('manageTask');
    });
    Route::get('/logout', 'HomeController@logout');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/getTasks', 'TaskController@getTasks');
    Route::post('/deleteTask', 'TaskController@deleteTask');
    Route::post("/upsertTask", 'TaskController@upsertTask');
    Route::post("/taskCompleted", 'TaskController@taskCompleted');
});

